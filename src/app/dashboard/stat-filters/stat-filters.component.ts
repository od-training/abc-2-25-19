import { Component, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { DashboardService } from '../dashboard.service';

@Component({
  selector: 'app-stat-filters',
  templateUrl: './stat-filters.component.html',
  styleUrls: ['./stat-filters.component.css']
})
export class StatFiltersComponent {
  search: FormControl;
  startDate = new FormControl(undefined, Validators.required);
  constructor(dashboardService: DashboardService) {
    this.search = dashboardService.search;
  }
}
