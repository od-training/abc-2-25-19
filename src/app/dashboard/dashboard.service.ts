import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormControl } from '@angular/forms';
import { combineLatest, of } from 'rxjs';
import { map, startWith, switchMap, debounceTime, pluck, filter, tap, onErrorResumeNext, catchError } from 'rxjs/operators';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Video } from './dashboard.types';
import { validateConfig } from '@angular/router/src/config';

@Injectable()
export class DashboardService {

  search = new FormControl();

  constructor(private http: HttpClient, private route: ActivatedRoute, router: Router) {
    this.search.valueChanges.subscribe(term => {
      router.navigate([], { queryParams: { filter: term } });
    });

    this.route.queryParams.pipe(
      pluck<Params, string>('filter')
    ).subscribe(term => this.search.setValue(term));
  }

  ssFilteredVideos() {
    return this.search.valueChanges
      .pipe(
        startWith(''),
        debounceTime(270),
        switchMap(search => this.http.get<Video[]>('http://localhost:8085/videos?q=' + search))
      );
  }

  ssFilteredVideos2() {
    return this.search.valueChanges
      .pipe(
        startWith(''),
        debounceTime(270),
        switchMap(search => {
          const params = new HttpParams().set('q', search);
          return this.http.get<Video[]>('http://localhost:8085/videos', { params });
        })
      );
  }

  filteredVideos() {
    const searchChanges = this.route.queryParams
      .pipe(
        pluck<Params, string>('filter'),
        filter(term => !!term),
        startWith('')
      );

    const apiRequest = this.http.get<Video[]>('http://localhost:8085/videos')

    return combineLatest(searchChanges, apiRequest)
      .pipe(
        map(([searchTerm, videoList]) => {
          return videoList.filter(video => video.title.toLowerCase().includes(searchTerm.toLowerCase()));
        })
      );
  }

}
