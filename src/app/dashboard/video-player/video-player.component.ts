import { Component, Input, OnChanges } from '@angular/core';

import { Video } from '../dashboard.types';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

const baseUrl = 'https://www.youtube.com/embed/';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.component.html',
  styleUrls: ['./video-player.component.css']
})
export class VideoPlayerComponent {
  @Input() set video(videoChange: Video) {
    if (videoChange) {
      this.title = videoChange.title;
      this.videoUrl = this.domSanitizer
        .bypassSecurityTrustResourceUrl(baseUrl + videoChange.id);
    }
  }
  title = '';
  videoUrl: SafeUrl;
  constructor(private domSanitizer: DomSanitizer) {

  }

}
