import { Component } from '@angular/core';
import { Video } from './dashboard.types';
import { DashboardService } from './dashboard.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {

  selectedVideo: Video | undefined;
  videoList: Observable<Video[]>;

  constructor(dashboardService: DashboardService) {
    this.videoList = dashboardService.filteredVideos();
  }

  setVideo(video: Video) {
    this.selectedVideo = video;
  }

}
