import { Component, Output, EventEmitter, Input } from '@angular/core';

import { Video } from '../dashboard.types';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent {
  @Input() selectedVideo: Video | undefined;
  @Input() videoList: Video[];
  @Output() videoSelected = new EventEmitter<Video>();

  selectVideo(video: Video) {
    this.videoSelected.emit(video);
  }

}
